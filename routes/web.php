<?php
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts/app');
});

Route::get('/gestion', function () {
    return view('layouts/app');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'AppMasterController@appmaster')->name('home');
Route::get('/login', 'MotoController@vista')->name('login');
//rutas para gestion
Route::get('/listarGestion', 'MotoController@listarClasificacionesItems')->name('listar-clasificaciones');
Route::post('/listarSubtipificaciones/{id}', 'MotoController@listarSubtipificaciones')->name('listar-sub-clasificaciones');
Route::post('/listarDatosCliente', 'MotoController@listarDatosCliente')->name('listar-datos');
Route::post('/gestion/{id}', 'MotoController@gestion')->name('gestion');
Route::put('/update/{id}', 'MotoController@update')->name('update');
Route::get('/datosGestion/{id}', 'MotoController@datosGestion')->name('datosGestion');
Route::post('/listarMetodoPago', 'MotoController@listarMetodoPago')->name('MetodoPago');
//rutas para calidad
Route::get('/listarDatosCalidad', 'MotoController@listarDatosCalidad')->name('listar-datos-calidad');
Route::get('/datosCalidadIndividual/{id}', 'MotoController@datosCalidadIndividual')->name('listar-datos-calidad-individuales');
Route::put('/guardarCalidad/{id}', 'MotoController@guardarCalidad')->name('guardar-calidad');
Route::get('/listarTipificacionesCalidad', 'MotoController@listarTipificacionesCalidad')->name('tipificaciones-calidad');
Route::get('/gestionesCalidad/{id}', 'MotoController@gestionesCalidad')->name('gestiones-calidad');
//rutas para devoluciones
Route::get('/listarDevoluciones', 'MotoController@listarDevoluciones')->name('listar-devoluciones');
Route::get('/listarInformacion/{id}', 'MotoController@listarInformacion')->name('listar-informacion-cliente');
Route::put('/tipificarDevolucion/{id}', 'MotoController@tipificarDevolucion')->name('tipificar-devolucion');
//rutas para descargar excel
Route::post('/descargarInforme', 'MotoController@descargarExcel')->name('descargar-informe');
//rutas para roles
Route::get('/traerRoles', 'MotoController@traerRoles')->name('traer-roles');
Route::post('/crearRol', 'MotoController@crearRol')->name('crear-roles');
Route::post('/borrarRol/{id}', 'MotoController@borrarRol')->name('borrar-roles');
//rutas para usuarios
Route::get('/listarUsuarios', 'MotoController@listarUsuarios')->name('listar-usuarios');
Route::put('/cambiarRol/{id}', 'MotoController@cambiarRol')->name('cambio-rol');
//rutas para cargar y ver excels
Route::post('/cargarArchivoExcelAdicional','CargaExcelController@cargarArchivoExcelAdicional')->name('cargar-archivo-excel-adicional');
Route::get('/listarArchivoCargaAdicional','MotoController@listarArchivoCargaAdicional')->name('listar-archivo-carga-adicional');
Route::get('/eliminarArchivo/{id}','MotoController@eliminarArchivo')->name('eliminar-archivo');
Route::put('/bloquearR/bloquearRegistros', 'MotoController@bloquearR')->name('bloquear-base');

//descargar archivos
Route::get('/file/download/{id}','MotoController@show')->name('downloadfile');











