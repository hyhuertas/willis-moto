<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gestion extends Model
{
    protected $table = 'gestion';
    public $fillable = [
        'id',
        'tipo_de_documento',
        'id_asegurado',
        'nombre_asegurado',
        'email_asegurado',
        'producto',
        'no_factura',
        'placa',
        'modelo',
        'marca',
        'fasecolda',
        'valor_asegurado_fasecolda',
        'no_chasis',
        'no_motor',
        'valor_prima_anual',
        'valor_prima_financiada',
        'servicio',
        'ciudad_circulacion_id',
        'departamento_id',
        'fecha_inicio_vigencia',
        'fecha_desembolso',
        'tipo_de_plan',
        'cobertura_deducibles',
        'telefono_celular',
        'direccion',
        'credito',
        'codigo_crm',
        'asesor_id',
        'autoriza_sms_voluntad',
        'user_asesor_id',
        'tipificacion_id',
        'subtipificacion_id',
        'detalle_id',
        'observacion_asesor',
        'user_calidad_id',
        'estado_inicial_calidad_id',
        'estado_final_calidad_id',
        'observacion_calidad',
        'id_archivo',
        'activo',
        'metodo_pago',
        'campana',
        'deducible_pt',
        'rc'    
    ];

    public function ciudad()
    {
        return $this->belongsTo('App\Models\Ciudades','ciudad_circulacion_id','codigo_dane');
    }

    public function departamento()
    {
        return $this->belongsTo('App\Models\Departamentos', 'departamento_id','codigo');
    }

    public function asesor()
    {
        return $this->belongsTo('App\User','codigo_crm', 'codigo_usercrm');
    }

    public function tipificacion()
    {    
        return $this->belongsTo('App\Models\ClasificacionItem','tipificacion_id', 'id');
    }

    public function subtipificacion()
    {    
        return $this->belongsTo('App\Models\ClasificacionItem','subtipificacion_id', 'id');
    }

    public function tipificacionCalidad()
    {
        return $this->belongsTo('App\Models\ClasificacionItem','estado_inicial_calidad_id','id');
    }

    public function tipificacionCalidadFinal()
    {
        return $this->belongsTo('App\Models\ClasificacionItem','estado_final_calidad_id','id');
    }

    public function asesorCalidad()
    {
        return $this->belongsTo('App\User','user_calidad_id', 'id');
    }

    public function metodoPago()
    {    
        return $this->belongsTo('App\Models\ClasificacionItem','metodo_pago', 'id');
    }
}
