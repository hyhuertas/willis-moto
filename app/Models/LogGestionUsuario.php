<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogGestionUsuario extends Model
{
    public $table = 'log_gestion_usuarios';

    protected $fillable = [
        'id',
        'gestion_id',
        'tipificacion_id',
        'subtipificacion_id',
        'detalle_id',
        'estado_id',
        'observacion',
        'rol',
        'codigo_crm'
    ];

    public function tipificacion()
    {    
        return $this->belongsTo('App\Models\ClasificacionItem','tipificacion_id', 'id');
    }

    public function subtipificacion()
    {    
        return $this->belongsTo('App\Models\ClasificacionItem','subtipificacion_id', 'id');
    }

    public function asesor()
    {
        return $this->belongsTo('App\User','codigo_crm', 'codigo_usercrm');
    }
}
