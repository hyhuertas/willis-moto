<?php

namespace App\Models\Excel;

use App\Models\Gestion;
use App\Models\LogGestionUsuario;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class ExporteReporteAsesores implements FromView
{
    public function __construct(string $fechaInicial, string $fechaFinal, $id)
    {
       $this->fechaInicio=$fechaInicial;
       $this->fechaFinal=$fechaFinal;
       $this->id=$id;
       return $this->getData();
    }
    /* funcion para el exporte de la data a los distinton informes y reportes */
    public function GetData()
    {
        $id=$this->id;
        $desde=$this->fechaInicio.' 00:00:00';
        $hasta=$this->fechaFinal.' 23:59:59';

        $gestion=Gestion::where('updated_at','>=', $desde)
                          ->where('updated_at','<=', $hasta)
                          ->where('activo','1');

        if($id==1){
            $gestiones=$gestion->get();                                    
        }
        if($id==2){
            $gestiones=$gestion->where('subtipificacion_id', 15)->get();                                      
        }
        if($id==3){
            $gestiones=$gestion->where('subtipificacion_id', 15)
                               ->whereNotNull('user_calidad_id')
                               ->get();                                   
        }
        return $gestiones;
        } 
        


    public function view(): View
    {
        $id=$this->id;
        if($id==1) {
            return view('reportes.excel-informe-gestion', [
                'gestiones' => $this->getData()
                ]);
        }     
        if($id==2) {
            return view('reportes.excel-reporte-ventas', [
                'gestiones' => $this->getData()
                ]);
        }
        if($id==3) {
            return view('reportes.excel-reporte-calidad', [
                'gestiones' => $this->getData()
                ]);
        }
    }
}
