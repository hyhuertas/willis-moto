<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    public $timestamps = false;
    protected $connection = 'master_connection';
    protected $table = 'roles';
    protected $fillable = ['id_rol','descripcion_rol', 'id_usuario', 'id_modulo', 'numero_rol'];
}
