<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClasificacionItem extends Model
{
    protected $table = 'clasificacion_items';
    protected $fillable = [];

    public function clasificacion()
    {    
        return $this->belongsTo('App\Models\Clasificacion','id');
    }
}
