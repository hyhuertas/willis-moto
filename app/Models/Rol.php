<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'roles';

    public $fillable = [
        'id',
        'descripcion',
        'rol',
        'descripcion_rol'
    ];
}
