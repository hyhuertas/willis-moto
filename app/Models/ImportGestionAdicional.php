<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Gestion;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;//para los nombres de las columnas
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;//para las reglas de validacion
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Throwable;//para el manejo de la excepcion
use Illuminate\Support\Facades\Auth;
use App\Models\ArchivoCarga;


class ImportGestionAdicional implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure, SkipsOnError,  WithChunkReading
{
    use Importable, SkipsFailures;
    public $rows = 0;
    public $fallos = 0;
    public $errores = 0;
    public $array_fallos=[];
    public $array_errores=[];
    public $backoffice_id='';
    public $rol='';
    public $id_archivo = '';
   
    public function __construct($backoffice_id, $rol, $id_archivo)
    {
     
        if ($id_archivo!=''){
            $this->id_archivo=$id_archivo;
        }
        
        if ($backoffice_id!=''){
            $this->backoffice_id=$backoffice_id;
        }elseif($backoffice_id==''){
            $this->backoffice_id=auth()->user()->codigo_usercrm;
        }

        if($this->backoffice_id == auth()->user()->codigo_usercrm){
            $this->rol=auth()->user()->rol_user_id;
        }else{
            $this->rol = $rol;
        }


    }
    public function model(array $row)
    {   

        $this->rows++;//para contabilizar el total de inserciones
        $this->fecha_v =  ($row['fecha_inicio_vigencia'] - 25569) * 86400;
        $this->fecha_vigencia = gmdate('Y-m-d H:i:s',$this->fecha_v);
        $this->fecha_d = ($row['fecha_desembolso'] - 25569) * 86400;
        $this->fecha_desembolso = gmdate('Y-m-d H:i:s',$this->fecha_d);

        return new Gestion([
            'tipo_de_documento'      => strval($row['tipo_de_documento']),
            'id_asegurado'      => strval($row['id_asegurado']),
            'nombre_asegurado'      => strval($row['nombre_asegurado']),
            'email_asegurado'      => strval($row['email_asegurado']),
            'producto'      => strval($row['producto']),
            'no_factura'      => strval($row['no_factura']),
            'placa'      => strval($row['placa']),
            'modelo'      => strval($row['modelo']),
            'marca'      => strval($row['marca']),
            'fasecolda'      => strval($row['fasecolda']),
            'valor_asegurado_fasecolda'      => strval($row['valor_asegurado_fasecolda']),
            'no_chasis'      => strval($row['no_chasis']),
            'no_motor'      => strval($row['no_motor']),
            'valor_prima_anual'      => strval($row['valor_prima_anual']),
            'valor_prima_financiada'      => strval($row['valor_prima_financiada']),
            'servicio'      => strval($row['servicio']),
            'ciudad_circulacion_id'      => strval($row['ciudad_circulacion']),
            'departamento_id'      => strval($row['departamento']),
            'fecha_inicio_vigencia'      => $this->fecha_vigencia,
            'fecha_desembolso'      => $this->fecha_desembolso,
            'tipo_de_plan'      => strval($row['tipo_de_plan']),
            'cobertura_deducibles'      => strval($row['cobertura_deducibles']),
            'telefono_celular'      => strval($row['telefono_celular']),
            'direccion'      => strval($row['direccion']),
            'credito'      => strval($row['credito']),
            'autoriza_sms_voluntad'      => strval($row['autoriza_sms_voluntad']),
            'rc'      => strval($row['rce']),
            'deducible_pt'      => strval($row['deducible_pt']),
            'campana'      => strval($row['campana']),
            'id_archivo' => $this->id_archivo
                    
        ]);

    }
    /**
     * Se indica que los datos comienzan desde la fila 2
     */

    public function headingRow(): int
    {
        return 1;
    }
    public function rules(): array
    {
        return [
            'fecha_inicio_vigencia' => 'required',
            'fecha_desembolso' => 'required',
            'placa' => 'required',
            'id_asegurado' => 'required',
            'campana' => 'required',
            //'ciudad_circulacion_id' => 'required|numeric',
            //'departamento_id' => 'required|numeric'
        ];
    }
      //para limitar el procesamiento a lote de la memoria
      public function chunkSize(): int
      {
          return 1000;
      }
      //para capturar los errores de validacion
      public function onFailure(Failure ...$failures)
      {
          foreach ($failures as $failure) {
            $this->fallos++;//para contabilizar el totl de fallos
              
              $this->array_fallos[$this->fallos-1]['registro']=$failure->row()-1;
              $this->array_fallos[$this->fallos-1]['columna']=$failure->attribute();
              $this->array_fallos[$this->fallos-1]['errors']=implode(' | ', $failure->errors());
  
          }
      }
      //para los errores o excepciones durante la inserción
      public function onError(Throwable $e)
      {         
          $rows=$this->rows;
          $this->errores++;//para contabilizar las excepciones
          $this->array_errores[$this->errores-1]['registro']= $rows + $this->headingRow();
          $this->array_errores[$this->errores-1]['errors']=$e->getMessage();        
      }
}
