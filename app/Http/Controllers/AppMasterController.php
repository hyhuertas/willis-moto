<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\Rol;
use App\Models\Roles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class AppMasterController extends Controller
{
    public function appmaster()
    {
        return view('layouts.app');
        $crm= $_GET['crm'];
        if (isset($_GET['phone_number'])) {            
            $telefono = $_GET['phone_number'];
        }else{
            $telefono = 0;
        }
        if (isset($_GET['unique_id'])) {
            $unique_id = $_GET['unique_id'];
        }else{
            $unique_id = null;
        }
 
        try {
             //$usuario_id= Crypt::decrypt($_GET['id_usuario']);
            $usuario_id=$_GET['idusuario'];

            if(isset($usuario_id)){
                $rol = Roles::where('id_usuario', '=', $usuario_id)
                ->where('id_modulo', '=',$crm)
                ->first();
    
                $usu = Usuario::where('id_usuario', '=', $usuario_id)
                ->where('estado', '=', 'Habilitado')
                ->first();
 
            
                $user=User::where('codigo_usercrm',$rol->id_usuario )->first();

                if(!isset($user)){//si no existe el usuario
                        $user= new User();//se define una nueva instancia
                        $user->password=Hash::make(123456);
                }
                
                //se actualiza o se crea el nuevo usuario
                $user->nombre=$usu->nombre_usuario.' '.$usu->apellido_usuario;
                $user->numero_documento=$usu->cedula_usuario;
                $user->codigo_usercrm=$usu->id_usuario;
                if ($user->rol_user_id==null) {
                    $user->rol_user_id=5;
                }
                elseif($user->rol_user_id!=5){
                    $user->rol_user_id=$user->rol_user_id;
                }
                if ($user->id==1 && $user->id==2 && $user->id==3) {
                    $user->rol_user_id=1;
                }             
                if ($user->id!=1) {
                    $user->visible=0;  
                }else{
                    $user->visible=1;
                }
                $user->save();
    
                Auth::login($user, true);
                $rolusu = $rol->numero_rol;
                $idusu = $usuario_id;
                $crm=$_GET['crm'] ?? session('crm');
                //se crean variable de sesion para redirigir a la vista de usuario
                session(['rolusu'=> $rolusu]);
                session(['idusu'=> $idusu]);
                session(['crm'=> $crm]);
                session(['telefono'=>$telefono]);
                session(['unique_id'=>$unique_id]);
    
                    return view('layouts.app',compact('idusu','crm','rolusu','telefono','unique_id'));

            }else{
                abort('404');
            }         
        } catch (DecryptException $e) {
           
            abort('404');
        }            
        }
    }
