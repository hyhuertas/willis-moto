<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\ClasificacionItem;
use Illuminate\Support\Facades\Auth;
use App\Models\LogGestionUsuario;
use App\Models\Excel\ExporteReporteAsesores;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Models\Rol;
use App\Models\Roles;
use App\User;
use App\Models\ArchivoCarga;
use Illuminate\Support\Facades\Storage;

class MotoController extends Controller
{
    public function index(){
        $moto = Gestion::get();
        return $moto;
    }

    public function vista(Request $request){

        return view('layouts/app');
    }

    public function listarClasificacionesItems(Request $request){

        $tipificaciones = ClasificacionItem::where('clasificacion_id', $request->clasificacion_id)->get();

        return $tipificaciones;
    }
    public function listarMetodoPago(Request $request){

        $metodoPago = ClasificacionItem::where('nivel', 3)->where('padre_id', 15)->where('clasificacion_id', 5)->get();

        return $metodoPago;
    }

    public function listarSubtipificaciones(Request $request){
        
        $subtipificaciones = ClasificacionItem::where('padre_id', $request->id)
        ->orderBy('prioridad', 'asc')->get();

        return $subtipificaciones;
    }

    public function listarDatosCliente(Request $request){
        $identificador = $request->item;

        $listarDatosCliente = Gestion::with('ciudad')->with('departamento')->where('activo','!=','0');

        if ($request->opc==1) {
            $validaDatos = $request->validate([
                'item' => 'required|min:6|max:12/[a-zA-Z0-9]{12}/',
            ]); 
            $listarDatosCliente=$listarDatosCliente->where('placa',$identificador)->where('activo','!=','0')
            ->orWhere('id_asegurado', $identificador)->where('activo','!=','0')
            ->orWhere('telefono_celular', $identificador)->where('activo','!=','0')->get();

        }else{
            $validaDatos = $request->validate([
                'item' => 'required|min:1|max:10/[0-9]/',
            ]); 

            $listarDatosCliente=$listarDatosCliente->Where('id', $identificador)->where('activo','!=','0')->get();
        }
                                                                              
        return $listarDatosCliente;                                    
    }

    public function update(Request $request, $id){

        $detalle_id = ClasificacionItem::where('padre_id', $request->subtipificacion)->first();

        if ($request->subtipificacion==15) {
            $validaDatos = $request->validate([
                'tipificacion' => 'required',
                'subtipificacion' => 'required',
                'metodoDePago' => 'required',
                'fecha' => 'required',
                'comentarios' => 'required|min:9|max:150'
                //'primerNombre' => 'required|min:3|max:20|regex:/^[A-Za-z]*$/',
            ]);
        }else{
            $validaDatos = $request->validate([
                'tipificacion' => 'required',
                'subtipificacion' => 'required',
                'comentarios' => 'required|min:9|max:150'
                //'primerNombre' => 'required|min:3|max:20|regex:/^[A-Za-z]*$/',
            ]);
        }        

        $gestion = Gestion::where('id', $id)->where('activo','1')->first();
        if (!is_null($gestion)) {
            $gestion->tipificacion_id = $request->tipificacion;
            $gestion->subtipificacion_id = $request->subtipificacion;
            if ($request->subtipificacion==15) {
                $gestion->estado_inicial_calidad_id = 50;
                $gestion->fecha_venta=Carbon::now()->format('Y-m-d H:i:s');
                $gestion->metodo_pago = $request->metodoDePago;                
                $gestion->fecha_inicio_de_vigencia = $request->fecha;                
            }
            $gestion->detalle_id = $detalle_id['id'];
            $gestion->observacion_asesor = $request->comentarios;
            $gestion->fecha_gestion = Carbon::now()->format('Y-m-d H:i:s');            
            $gestion->codigo_crm = auth()->user()->codigo_usercrm;
            $gestion->asesor_id = auth()->user()->id;
            if ($request->unique_id!=null) {                
                $gestion->unique_id = $request->unique_id;
            }

            $logGestion = new LogGestionUsuario();
            $logGestion->gestion_id = $gestion->id;
            $logGestion->tipificacion_id = $request->tipificacion;
            $logGestion->subtipificacion_id = $request->subtipificacion;
            $logGestion->detalle_id = $detalle_id['id'];
            $logGestion->estado_id = 1;
            $logGestion->observacion = $request->comentarios;
            $logGestion->rol_id = auth()->user()->rol_user_id;
            $logGestion->codigo_crm = auth()->user()->codigo_usercrm;
            if ($request->unique_id!=null) {                
                $logGestion->unique_id = $request->unique_id;
            }
            $logGestion->save();
            $gestion->save();
            
        }
        
        return $logGestion;
    }

    public function datosGestion(Request $request, $id){
        $gestiones = LogGestionUsuario::with('tipificacion')->with('subtipificacion')->with('asesor')
        ->where('gestion_id', $request->id)->where('tipificacion_id','<',24)->get();
        
        return $gestiones;
    }

    public function listarDatosCalidad(Request $request){

        $listarDatosCalidad = Gestion::with('tipificacionCalidad')->with('tipificacionCalidadFinal')->with('asesor')->where('subtipificacion_id', 15)->where('estado_inicial_calidad_id', '!=', 48)->whereNull('estado_final_calidad_id')->with('asesorCalidad')->where('activo','1')->get(); 
        
        return $listarDatosCalidad;                                    
    }

    public function datosCalidadIndividual(Request $request, $id){   
        
        $datosCalidadIndividual = Gestion::where('id',$request->id)->with('ciudad')->with('departamento')->with('tipificacion')->with('subtipificacion')->with('asesor')->with('metodoPago')
                                         ->where('subtipificacion_id','=', 15)->first();                                                  
        return $datosCalidadIndividual;                                    
    }

    public function listarTipificacionesCalidad(){
        $listarTipificacionesCalidad = ClasificacionItem::where('clasificacion_id', 4)->get();

        return $listarTipificacionesCalidad;
    }


    public function guardarCalidad(Request $request, $id){
              
            $validaDatos = $request->validate([
                'comentarios' => 'required|min:9|max:150',
                'tipificacion' => 'required'
                //'primerNombre' => 'required|min:3|max:20|regex:/^[A-Za-z]*$/',
            ]);

            $gestion = Gestion::where('id',$request->gestion_id)->first();


            $gestion->user_calidad_id = auth()->user()->id;
            if ($request->tipificacion!=45) {
                $gestion->estado_inicial_calidad_id = $request->tipificacion;                
            }
            if ($request->tipificacion==45) {
                $gestion->estado_final_calidad_id = $request->tipificacion;
            }
            $gestion->observacion_calidad = $request->comentarios;                        
            
            $logGestion = new LogGestionUsuario();
            $logGestion->gestion_id = $gestion->id;
            $logGestion->tipificacion_id = $request->tipificacion;
            if ($request->tipificacion_id==45) {
                $logGestion->subtipificacion_id = $request->tipificacion;                
            }else{
                $logGestion->subtipificacion_id = 1;                
            }
            $logGestion->detalle_id = $gestion->detalle_id;
            $logGestion->estado_id = 1;
            $logGestion->observacion = $request->comentarios;
            $logGestion->rol_id = auth()->user()->rol_user_id;
            $logGestion->codigo_crm = auth()->user()->codigo_usercrm;
            $logGestion->save();
            $gestion->save();
        
        return $logGestion;
    }

    public function gestionesCalidad(Request $request, $id){
        $gestionesCalidad = LogGestionUsuario::with('tipificacion')->with('subtipificacion')->with('asesor')
                                             ->where('gestion_id',$id)
                                             ->where('tipificacion_id','>', 43)
                                             ->get();

        return $gestionesCalidad;
    }

    public function listarDevoluciones()
    {
        $id_usuario = auth()->user()->id;

        $listarDevoluciones = Gestion::where('subtipificacion_id', 15)->whereIn('estado_inicial_calidad_id',[44,48])
                                     ->where('asesor_id', $id_usuario)->where('activo', '1')
                                     ->get();

        return $listarDevoluciones;
    }
    
    public function listarInformacion(Request $request,$id)
    {
        $listarInformacion = Gestion::with('ciudad')->with('tipificacionCalidad')
                                    ->with('departamento')->with('tipificacion')
                                    ->with('subtipificacion')->with('asesor')
                                    ->with('asesorCalidad')->with('metodoPago')
                                    ->where('activo', '1')
                                    ->where('id',$request->id)->first();                        
        return $listarInformacion;
    }

    public function tipificarDevolucion(Request $request, $id)
    {
        $validaDatos = $request->validate([
            'comentarios' => 'required|min:9|max:150',
            'metodopago' => 'required',
            'fecha' => 'required'
            //'primerNombre' => 'required|min:3|max:20|regex:/^[A-Za-z]*$/',            
        ]);

        $gestion = Gestion::where('id',$request->id)->where('activo','1')->first();

        $gestion->user_calidad_id = auth()->user()->id;
        $gestion->estado_inicial_calidad_id = 49;                
        $gestion->observacion_asesor = $request->comentarios;
        $gestion->metodo_pago = $request->metodopago;
        $gestion->fecha_inicio_de_vigencia = $request->fecha;
        
        $logGestion = new LogGestionUsuario();
        $logGestion->gestion_id = $gestion->id;
        $logGestion->tipificacion_id = $gestion->estado_inicial_calidad_id;
        $logGestion->subtipificacion_id = $gestion->estado_inicial_calidad_id;                
        $logGestion->detalle_id = $gestion->estado_inicial_calidad_id;
        $logGestion->estado_id = 1;
        $logGestion->observacion = $request->comentarios;
        $logGestion->rol_id = auth()->user()->rol_user_id;
        $logGestion->codigo_crm = auth()->user()->codigo_usercrm;
        $logGestion->save();
        $gestion->save();
        
        return $logGestion;
    }

    public function descargarExcel(Request $request){
        
        $fechaInicial=$request->fechaInicial;
        $fechaFinal=$request->fechaFinal;
        $id=$request->id;

        $fecha=date('Y-m-d');
        $reporte=new ExporteReporteAsesores($fechaInicial,  $fechaFinal, $id);
        return Excel::download($reporte, 'Reporte_general_'.$fecha.'.xlsx');
    }

    public function traerRoles(){

        $roles = Rol::get();

        return $roles;
    }

    public function listarUsuarios(){
        $usuarios = User::with('rol')->where('visible', 0)->get();
        return $usuarios;
    }

    public function cambiarRol(Request $request, $id){

        $cambioRol = User::find($id);
        $cambioRol->rol_user_id = $request->rol;

        $cambioRolMaster = Roles::where('id_usuario',$request->idusu)
                                ->where('id_modulo',session('crm'))
                                ->update(['numero_rol'=>$request->rol]);
        $cambioRol->save();

        return $cambioRol;
    }

    public function listarArchivoCargaAdicional(){

        $archivoCargaAdicional=ArchivoCarga::orderBy('created_at','DESC')
           ->get();
         return $archivoCargaAdicional;       
    }

    public function eliminarArchivo(Request $request, $id){
        
        $dropArchivo = ArchivoCarga::where('nombre_archivo',$id);
        $file2 = unlink(storage_path('app/public/Imports/'.$id));         
        $dropArchivo->delete(); 
           
    }
    public function bloquearR(Request $request){

        ArchivoCarga::where('id', $request->id)
                    ->update(['activo' => $request->param]);

        Gestion::where('id_archivo', $request->id)
                ->update(['activo' => $request->param]);
    
    }

    public function show($id){
        $dl = ArchivoCarga::where('nombre_archivo', $id);
        $file2 = unlink(storage_path('app/public/Imports/'.$id));         
        return Storage::download($file2);
    }
}
