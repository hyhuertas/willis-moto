<?php

namespace App\Http\Controllers;

use App\Exports\CargueExport;
use App\Models\ArchivoCarga;
use App\Models\Gestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Models\ImportGestionAdicional;


class CargaExcelController extends Controller
{
    public function cargarArchivoExcelAdicional(Request $request){
        DB::beginTransaction();
        
        try {
            $rol=auth()->user()->rol_user_id;
            $backoffice_id=auth()->user()->codigo_usercrm;
            $extension_permitidas=['xlsx','ods','xls'];
            $file=$request->file('file');

           
            $ext = $file->getClientOriginalExtension();//extension del archivo
            if(!in_array($ext,$extension_permitidas)){
                return response()->json(['errors'=>['file'=>'El formato y extensión del archivo debe ser .xlsx, .xls, .ods']], 422);
            }
            $store="Imports";
            $modelo="";
            $diskErrors="erroresWillis";

               

            
            $random_name = $this->generateRandomString().".xlsx";
            $path=$file->storeAs($store,$random_name);
            $archivoCarga=new ArchivoCarga();
            $archivoCarga->nombre_original = $file->getClientOriginalName();
            $archivoCarga->nombre_archivo =  $random_name;
            $archivoCarga->ruta =  $path;
            $archivoCarga->id_usuario = $backoffice_id;
            // 
            // $total=0;
            
            $archivoCarga->numero_registros_cargados=0;
            $archivoCarga->save();


            $modelo=new ImportGestionAdicional($backoffice_id, $rol, $archivoCarga->id);
            $import=$modelo; //se instancia la clase de importacion
            Excel::import( $import,$file);
            
            $rows=$import->rows;

            $updateArhivo = ArchivoCarga::where('id', $archivoCarga->id)
            ->update([
                'numero_registros_cargados' => $rows
            ]);
            if($import->errores>0 || $import->fallos>0){//si existe un errores de excepcion durante la inserción
                $random_name2 = $this->generateRandomString().".xlsx";
                
                $fileErrors=Excel::store(new CargueExport($import->array_fallos,$import->array_errores),$random_name2,$diskErrors);
                
                $pathErrors=Storage::disk($diskErrors)->url($random_name2);
                $archivoCarga->numero_errores=$import->errores;
                $archivoCarga->ruta_errores=$pathErrors;
                $archivoCarga->numero_fallas=$import->fallos;
                $archivoCarga->save();
            }        

                     
            

            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);

        }
    }
    public function generateRandomString($length = 8) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
}