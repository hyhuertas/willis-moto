<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogGestionUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_gestion_usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('gestion_id');
            $table->foreign('gestion_id')->references('id')->on('gestion');
            $table->unsignedBigInteger('tipificacion_id');
            $table->foreign('tipificacion_id')->references('id')->on('clasificacion_items');
            $table->unsignedBigInteger('subtipificacion_id');
            $table->foreign('subtipificacion_id')->references('id')->on('clasificacion_items');
            $table->unsignedBigInteger('detalle_id');
            $table->foreign('detalle_id')->references('id')->on('clasificacion_items');
            $table->unsignedBigInteger('estado_id');
            $table->foreign('estado_id')->references('id')->on('clasificacion_items');
            $table->longText('observacion')->nullable();
            $table->string('rol_id')->nullable();
            $table->string('codigo_crm')->nullable();
            $table->string('unique_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_gestion_usuarios');
    }
}
