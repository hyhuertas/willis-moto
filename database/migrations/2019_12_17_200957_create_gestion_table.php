<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /* migracion para el cargue de base excel */
    public function up()
    {
        Schema::create('gestion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipo_de_documento');
            $table->string('id_asegurado');
            $table->string('nombre_asegurado');
            $table->string('email_asegurado')->nullable();
            $table->string('producto')->nullable();
            $table->string('no_factura')->nullable();
            $table->string('placa');
            $table->string('modelo')->nullable();
            $table->string('marca',40)->nullable();
            $table->string('fasecolda')->nullable();
            $table->string('valor_asegurado_fasecolda',50)->nullable();
            $table->string('no_chasis')->nullable();
            $table->string('no_motor')->nullable();
            $table->string('valor_prima_anual',50)->nullable();
            $table->string('valor_prima_financiada',50)->nullable();
            $table->string('servicio')->nullable();
            $table->string('ciudad_circulacion_id');
            $table->string('departamento_id');
            $table->date('fecha_inicio_vigencia')->nullable();
            $table->date('fecha_desembolso')->nullable();
            $table->string('tipo_de_plan')->nullable();
            $table->string('cobertura_deducibles')->nullable();
            $table->string('telefono_celular')->nullable();
            $table->string('direccion')->nullable();
            $table->string('credito')->nullable();
            $table->string('rc')->nullable();
            $table->string('deducible_pt')->nullable();
            $table->string('campana');
            $table->string('codigo_crm')->nullable();
            $table->integer('asesor_id')->nullable();
            $table->string('autoriza_sms_voluntad')->nullable();
            $table->unsignedBigInteger('tipificacion_id')->nullable();
            $table->foreign('tipificacion_id')->references('id')->on('clasificacion_items');
            $table->unsignedBigInteger('subtipificacion_id')->nullable();
            $table->foreign('subtipificacion_id')->references('id')->on('clasificacion_items');
            $table->unsignedBigInteger('detalle_id')->nullable();
            $table->foreign('detalle_id')->references('id')->on('clasificacion_items');
            $table->unsignedBigInteger('metodo_pago')->nullable();
            $table->foreign('metodo_pago')->references('id')->on('clasificacion_items');
            $table->dateTime('fecha_gestion')->nullable();
            $table->dateTime('fecha_venta')->nullable();
            $table->date('fecha_inicio_de_vigencia')->nullable();
            $table->longText('observacion_asesor',150)->nullable();
            $table->unsignedBigInteger('user_calidad_id')->nullable();
            $table->foreign('user_calidad_id')->references('id')->on('users');
            $table->unsignedBigInteger('estado_inicial_calidad_id')->nullable();        
            $table->foreign('estado_inicial_calidad_id')->references('id')->on('clasificacion_items');
            $table->unsignedBigInteger('estado_final_calidad_id')->nullable();        
            $table->foreign('estado_final_calidad_id')->references('id')->on('clasificacion_items');
            $table->longText('observacion_calidad',150)->nullable();        
            $table->string('unique_id')->nullable();
            $table->unsignedBigInteger('id_archivo')->nullable();
            $table->foreign('id_archivo')->references('id')->on('archivos_cargas');
            $table->boolean('activo')->default(1)->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gestion');
    }
}
