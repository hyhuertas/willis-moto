<?php

use Illuminate\Database\Seeder;

class ClasificacionItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clasificacion_items')->delete();
        
        \DB::table('clasificacion_items')->insert(array (
            0 => 
            array (
                'item' => 'Contactado',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 1,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'item' => 'No Contactado',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 1,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'item' => 'Contacto Con Tercero',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 1,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'item' => 'Bloqueado-no volver a llamar al cliente',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 4,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'item' => 'Cliente informa no ser el propietario de la moto',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 3,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'item' => 'Cliente informa que nunca tuvo la poliza con el banco',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 3,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
            'item' => 'Cliente tuvo siniestro (hurto o daños a la moto)',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 3,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'item' => 'Cliente vendera o vendio la moto',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 3,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'item' => 'Le parecio muy cosotosa la poliza ofrecida',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 3,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'item' => 'No le gusta la aseguradora',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 3,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'item' => 'No le gusta la forma de pago',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 3,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'item' => 'No le interesa la poliza ofrecida',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 3,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'item' => 'Cliente manifiesta estar ocupado - volver a contactar',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 3,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'item' => 'Volver a llamar contacto efectivo',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 4,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'item' => 'Venta',
                'nivel' => '2',
                'padre_id' => 1,
                'clasificacion_id' => 2,
                'prioridad' => 1,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
            'item' => 'No responde (no es posible dejar mensaje de voz)',
                'nivel' => '2',
                'padre_id' => 2,
                'clasificacion_id' => 2,
                'prioridad' => 4,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
            'item' => 'No responde (se deja mensaje de voz)',
                'nivel' => '2',
                'padre_id' => 2,
                'clasificacion_id' => 2,
                'prioridad' => 4,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'item' => 'Dañado',
                'nivel' => '2',
                'padre_id' => 2,
                'clasificacion_id' => 2,
                'prioridad' => 4,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'item' => 'Desactualizado',
                'nivel' => '2',
                'padre_id' => 2,
                'clasificacion_id' => 2,
                'prioridad' => 4,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'item' => 'Contacto no se encuentra',
                'nivel' => '2',
                'padre_id' => 3,
                'clasificacion_id' => 2,
                'prioridad' => 4,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'item' => 'Volver a llamar contacto no efectivo',
                'nivel' => '2',
                'padre_id' => 3,
                'clasificacion_id' => 2,
                'prioridad' => 4,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'item' => 'Telefono correcto pero informa que el cliente no se encuentra',
                'nivel' => '2',
                'padre_id' => 3,
                'clasificacion_id' => 2,
                'prioridad' => 4,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'item' => 'Cliente falleció',
                'nivel' => '2',
                'padre_id' => 3,
                'clasificacion_id' => 2,
                'prioridad' => 4,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 4,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 5,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 6,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 7,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 8,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 9,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 10,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 11,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 12,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 15,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 18,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 19,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'item' => 'NO REINTENTAR',
                'nivel' => '0',
                'padre_id' => 23,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'item' => 'AGENDA',
                'nivel' => '0',
                'padre_id' => 13,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'item' => 'AGENDA',
                'nivel' => '0',
                'padre_id' => 14,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'item' => 'REINTENTO AUTOMATICO',
                'nivel' => '0',
                'padre_id' => 16,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'item' => 'REINTENTO AUTOMATICO',
                'nivel' => '0',
                'padre_id' => 17,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'item' => 'REINTENTO AUTOMATICO',
                'nivel' => '0',
                'padre_id' => 20,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'item' => 'REINTENTO AUTOMATICO',
                'nivel' => '0',
                'padre_id' => 21,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'item' => 'REINTENTO AUTOMATICO',
                'nivel' => '0',
                'padre_id' => 22,
                'clasificacion_id' => 3,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'item' => 'NC-Incompleta',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 4,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'item' => 'SC-Correcta',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 4,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'item' => 'SC-Rechazada en primera revision',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 4,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'item' => 'SC-Rechazada en segunda revision',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 4,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'item' => 'SC-Devuelta',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 4,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'item' => 'SC-Corregida',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 4,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'item' => 'SC-Sin revisar',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 4,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'item' => 'SC-Nunca fue recuperada',
                'nivel' => '1',
                'padre_id' => 0,
                'clasificacion_id' => 4,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'item' => 'Financiado banco',
                'nivel' => '3',
                'padre_id' => 15,
                'clasificacion_id' => 5,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'item' => 'Financiado débito',
                'nivel' => '3',
                'padre_id' => 15,
                'clasificacion_id' => 5,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'item' => 'Financiado pse',
                'nivel' => '3',
                'padre_id' => 15,
                'clasificacion_id' => 5,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'item' => 'Contado banco',
                'nivel' => '3',
                'padre_id' => 15,
                'clasificacion_id' => 5,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'item' => 'Contado pse',
                'nivel' => '3',
                'padre_id' => 15,
                'clasificacion_id' => 5,
                'prioridad' => 0,
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}