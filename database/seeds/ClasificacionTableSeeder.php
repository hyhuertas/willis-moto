<?php

use Illuminate\Database\Seeder;

class ClasificacionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clasificacion')->delete();
        
        \DB::table('clasificacion')->insert(array (
            0 => 
            array (
                'nombre' => 'tipificacion',
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'nombre' => 'subtipificacion',
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'nombre' => 'detalle',
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'nombre' => 'tipificacion_calidad',
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'nombre' => 'metodos de pago',
                'activo' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}