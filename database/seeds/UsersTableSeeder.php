<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nombre' => 'HECTOR YESID HUERTAS SEGURA',
            'codigo_usercrm' => '16769',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'rol_user_id' => 1,
            'visible' => 1,
            'remember_token' => NULL
        ]);
        DB::table('users')->insert([
            'nombre' => 'MARIA ELCY CORTES CANACUE',
            'codigo_usercrm' => '15753',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'rol_user_id' => 1,
            'visible' => 0,
            'remember_token' => NULL
        ]);
        DB::table('users')->insert([
            'nombre' => 'ADRIANA CRISTINA CASTRO BALLEN',
            'codigo_usercrm' => '15795',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'rol_user_id' => 1,
            'visible' => 0,
            'remember_token' => NULL
        ]);
    }
}
