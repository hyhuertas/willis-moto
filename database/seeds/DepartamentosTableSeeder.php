<?php

use Illuminate\Database\Seeder;
use App\Models\Departamentos;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = Departamentos::all()->count();

        if ($count == 0) {
            Departamentos::create(['codigo' => 5, 'nombre' => 'ANTIOQUIA']);
            Departamentos::create(['codigo' => 8, 'nombre' => 'ATLANTICO']);
            Departamentos::create(['codigo' => 11, 'nombre' => 'BOGOTA, D.C.']);
            Departamentos::create(['codigo' => 13, 'nombre' => 'BOLIVAR']);
            Departamentos::create(['codigo' => 15, 'nombre' => 'BOYACA']);
            Departamentos::create(['codigo' => 17, 'nombre' => 'CALDAS']);
            Departamentos::create(['codigo' => 18, 'nombre' => 'CAQUETA']);
            Departamentos::create(['codigo' => 19, 'nombre' => 'CAUCA']);
            Departamentos::create(['codigo' => 20, 'nombre' => 'CESAR']);
            Departamentos::create(['codigo' => 23, 'nombre' => 'CORDOBA']);
            Departamentos::create(['codigo' => 25, 'nombre' => 'CUNDINAMARCA']);
            Departamentos::create(['codigo' => 27, 'nombre' => 'CHOCO']);
            Departamentos::create(['codigo' => 41, 'nombre' => 'HUILA']);
            Departamentos::create(['codigo' => 44, 'nombre' => 'LA GUAJIRA']);
            Departamentos::create(['codigo' => 47, 'nombre' => 'MAGDALENA']);
            Departamentos::create(['codigo' => 50, 'nombre' => 'META']);
            Departamentos::create(['codigo' => 52, 'nombre' => 'NARIÑO']);
            Departamentos::create(['codigo' => 54, 'nombre' => 'NORTE DE SANTANDER']);
            Departamentos::create(['codigo' => 63, 'nombre' => 'QUINDIO']);
            Departamentos::create(['codigo' => 66, 'nombre' => 'RISARALDA']);
            Departamentos::create(['codigo' => 68, 'nombre' => 'SANTANDER']);
            Departamentos::create(['codigo' => 70, 'nombre' => 'SUCRE']);
            Departamentos::create(['codigo' => 73, 'nombre' => 'TOLIMA']);
            Departamentos::create(['codigo' => 76, 'nombre' => 'VALLE DEL CAUCA']);
            Departamentos::create(['codigo' => 81, 'nombre' => 'ARAUCA']);
            Departamentos::create(['codigo' => 85, 'nombre' => 'CASANARE']);
            Departamentos::create(['codigo' => 86, 'nombre' => 'PUTUMAYO']);
            Departamentos::create(['codigo' => 88, 'nombre' => 'ARCHIPIELAGO DE SAN ANDRES, PROVIDENCIA Y SANTA CATALINA']);
            Departamentos::create(['codigo' => 91, 'nombre' => 'AMAZONAS']);
            Departamentos::create(['codigo' => 94, 'nombre' => 'GUAINIA']);
            Departamentos::create(['codigo' => 95, 'nombre' => 'GUAVIARE']);
            Departamentos::create(['codigo' => 97, 'nombre' => 'VAUPES']);
            Departamentos::create(['codigo' => 99, 'nombre' => 'VICHADA']);
        }
    }
}
