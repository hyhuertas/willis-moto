<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'descripcion' => 'Administrador',
                'rol' => '1',
                'descripcion_rol' => 'Acceso a todo el proyecto y sus funciones',
            ),
            1 => 
            array (
                'descripcion' => 'Asesor',
                'rol' => '2',
                'descripcion_rol' => 'Acceso a la opcion de gestion',
            ),
            2 => 
            array (
                'descripcion' => 'Calidad',
                'rol' => '3',
                'descripcion_rol' => 'Acceso a la opcion de calidad',
            ),
            3 => 
            array (
                'descripcion' => 'Bases-Informes',
                'rol' => '4',
                'descripcion_rol' => 'Acceso a la funcion para descargar reportes e informes y subir bases de datos',
            ),
            4 => 
            array (
                'descripcion' => 'Inicial',
                'rol' => '5',
                'descripcion_rol' => 'Sin acceso',
            ),
        ));
        
        
    }
}