/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//window.Vue = require('vue');
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Vue from 'vue'


window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.use(Vuetify)
Vue.use(VueRouter)

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('menu-component', require('./components/MenuComponent.vue').default);
Vue.component('gestion-component', require('./components/GestionComponent.vue').default);
Vue.component('informes-component', require('./components/InformesReportesComponent.vue').default);
Vue.component('bases-component', require('./components/BasesComponent.vue').default);
Vue.component('calidad-component', require('./components/CalidadComponent.vue').default);
Vue.component('roles-component', require('./components/RolesComponent.vue').default);
Vue.component('usuarios-component', require('./components/UsuariosComponent.vue').default);
Vue.component('tipificaciones-component', require('./components/TipificacionesComponent.vue').default);
Vue.component('busqueda-component', require('./search/BusquedaComponent.vue').default);
Vue.component('dashboard-component', require('./components/DashboardComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
let home = { template: `<menu-component></menu-component>` }

const router = new VueRouter({
    routes: [{
        path: '/home',
        name: 'Inicio',
        component: home,
    }
    ],
    mode: 'history'
});


const app = new Vue({
    router,
    el: '#app',    
    vuetify: new Vuetify({
        icons: {
            iconfont: 'md',
        },
    }),
});

