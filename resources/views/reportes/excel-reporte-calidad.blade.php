<table>
    <thead>
        <tr>
            <td colspan="26" style="text-align:center; color: #2a502b; background: #fffdc7;">{{ strtoupper('Base Cargada') }}</td>
            <td colspan="9" style="text-align:center; color: #2a502b; background: #93ff2e;">{{ strtoupper('Gestion') }}</td>
        </tr>
        <tr>
            <td style="text-align:center; color: #4CAF50">Tipo de Documento</td>
            <td style="text-align:center; color: #4CAF50">ID Asegurado</td>
            <td style="text-align:center; color: #4CAF50">Nombre Asegurado</td>
            <td style="text-align:center; color: #4CAF50">Email Asegurado</td>
            <td style="text-align:center; color: #4CAF50">Producto</td>
            <td style="text-align:center; color: #4CAF50">No. Factura</td>
            <td style="text-align:center; color: #4CAF50">PLACA</td>
            <td style="text-align:center; color: #4CAF50">Modelo</td>
            <td style="text-align:center; color: #4CAF50">Marca</td>
            <td style="text-align:center; color: #4CAF50">Fasecolda</td>
            <td style="text-align:center; color: #4CAF50">Valor Asegurado Fasecolda</td> 
            <td style="text-align:center; color: #4CAF50">No. Chasis</td> 
            <td style="text-align:center; color: #4CAF50">No. Motor</td> 
            <td style="text-align:center; color: #4CAF50">Vr Prima Anual</td>           
            <td style="text-align:center; color: #4CAF50">Vr Prima Financiada</td>
            <td style="text-align:center; color: #4CAF50">Servicio</td>
            <td style="text-align:center; color: #4CAF50">Ciudad Circulacion</td>
            <td style="text-align:center; color: #4CAF50">Departamento</td>
            <td style="text-align:center; color: #4CAF50">Fe. Inicio Vigencia</td>
            <td style="text-align:center; color: #4CAF50">Fecha Desembolso</td>
            <td style="text-align:center; color: #4CAF50">tipo de plan</td>
            <td style="text-align:center; color: #4CAF50">Cobertura/Deducibles</td>
            <td style="text-align:center; color: #4CAF50">Telefono Celular</td>
            <td style="text-align:center; color: #4CAF50">Direccion</td>
            <td style="text-align:center; color: #4CAF50">Credito</td>
            <td style="text-align:center; color: #4CAF50">Autoriza SMS a Voluntad</td>
            <td style="text-align:center; color: #4CAF50">Tipificacion</td>
            <td style="text-align:center; color: #4CAF50">Sub-Tipificacion</td>
            <td style="text-align:center; color: #4CAF50">Observacion Asesor</td>
            <td style="text-align:center; color: #4CAF50">Fecha Ultima Gestion</td>
            <td style="text-align:center; color: #4CAF50">Asesor Gestion</td>
            <td style="text-align:center; color: #4CAF50">Estado inicial de la Venta</td>
            <td style="text-align:center; color: #4CAF50">Estado final de la Venta</td>
            <td style="text-align:center; color: #4CAF50">Comentario Calidad</td>
            <td style="text-align:center; color: #4CAF50">Asesor Calidad</td>
        </tr>
    </thead>
    <tbody>        
        @foreach ($gestiones as $gestion)
            <tr>                  
                <th>{{ is_null($gestion->tipo_de_documento) ? 'NO APLICA' :  $gestion->tipo_de_documento}}</th>
                <th>{{ is_null($gestion->id_asegurado) ? 'NO APLICA' : $gestion->id_asegurado }}</th>
                <th>{{ is_null($gestion->nombre_asegurado) ? 'NO APLICA' : $gestion->nombre_asegurado }}</th>
                <th>{{ is_null($gestion->email_asegurado) ? 'NO APLICA' : $gestion->email_asegurado }}</th>
                <th>{{ is_null($gestion->producto) ? 'NO APLICA' : $gestion->producto }}</th>
                <th>{{ is_null($gestion->no_factura) ? 'NO APLICA' : $gestion->no_factura }}</th>
                <th>{{ is_null($gestion->placa) ? 'NO APLICA' : $gestion->placa }}</th>
                <th>{{ is_null($gestion->modelo) ? 'NO APLICA' : $gestion->modelo }}</th>
                <th>{{ is_null($gestion->marca) ? 'NO APLICA' : $gestion->marca }}</th>
                <th>{{ is_null($gestion->fasecolda) ? 'NO APLICA' : $gestion->fasecolda }}</th>
                <th>{{ is_null($gestion->valor_asegurado_fasecolda) ? 'NO APLICA' : $gestion->valor_asegurado_fasecolda }}</th>
                <th>{{ is_null($gestion->no_chasis) ? 'NO APLICA' : $gestion->no_chasis }}</th>
                <th>{{ is_null($gestion->no_motor) ? 'NO APLICA' : $gestion->no_motor }}</th>
                <th>{{ is_null($gestion->valor_prima_anual) ? 'NO APLICA' : $gestion->valor_prima_anual }}</th>
                <th>{{ is_null($gestion->valor_prima_financiada) ? 'NO APLICA' : $gestion->valor_prima_financiada }}</th>  
                <th>{{ is_null($gestion->servicio) ? 'NO APLICA' : $gestion->servicio }}</th>
                <th>{{ is_null($gestion->ciudad->nombre) ? 'NO APLICA' : $gestion->ciudad->nombre }}</th>
                <th>{{ is_null($gestion->departamento->nombre) ? 'NO APLICA' : $gestion->departamento->nombre }}</th>
                <th>{{ is_null($gestion->fecha_inicio_vigencia) ? 'NO APLICA' : $gestion->fecha_inicio_vigencia }}</th>
                <th>{{ is_null($gestion->fecha_desembolso) ? 'NO APLICA' : $gestion->fecha_desembolso }}</th>
                <th>{{ is_null($gestion->tipo_de_plan) ? 'NO APLICA' : $gestion->tipo_de_plan }}</th>
                <th>{{ is_null($gestion->cobetura_deducibles) ? 'NO APLICA' : $gestion->cobetura_deducibles }}</th>
                <th>{{ is_null($gestion->telefono_celular) ? 'NO APLICA' : $gestion->telefono_celular }}</th>
                <th>{{ is_null($gestion->direccion) ? 'NO APLICA' : $gestion->direccion }}</th>
                <th>{{ is_null($gestion->credito) ? 'NO APLICA' : $gestion->credito }}</th>
                <th>{{ is_null($gestion->autoriza_sms_voluntad) ? 'NO APLICA' : $gestion->autoriza_sms_voluntad }}</th>
                <th>{{ is_null($gestion->tipificacion_id) ? 'NO APLICA' : $gestion->tipificacion->item }}</th>
                <th>{{ is_null($gestion->subtipificacion_id) ? 'NO APLICA' : $gestion->subtipificacion->item }}</th>
                <th>{{ is_null($gestion->observacion_asesor) ? 'NO APLICA' : $gestion->observacion_asesor }}</th>
                <th>{{ is_null($gestion->fecha_gestion) ? 'NO APLICA' : $gestion->fecha_gestion }}</th>                
                <th>{{ is_null($gestion->codigo_crm) ? 'NO APLICA' : $gestion->asesor->nombre }}</th>
                <th>
                @if(!is_null($gestion->estado_inicial_calidad_id))               
                {{ $gestion->tipificacionCalidad->item }}               
                @else
                {{ 'NO APLICA' }}
                @endif        
                </th>  
                <th>
                @if(!is_null($gestion->estado_final_calidad_id))               
                {{ $gestion->tipificacionCalidadFinal->item }}               
                @else
                {{ 'NO APLICA' }}
                @endif        
                </th> 
                <th>{{is_null($gestion->observacion_calidad) ? 'NO APLICA' : $gestion->observacion_calidad}}</th>      
                <th>{{is_null($gestion->user_calidad_id) ? 'NO APLICA' : $gestion->asesorCalidad->numero_documento}}</th>      
            </tr>
        @endforeach 
    </tbody>
</table>