<table>
    <thead>
        <tr>
            <td style="text-align:center; background-color: #4CAF50">Fecha_venta</td>
            <td style="text-align:center; background-color: #4CAF50">cedula</td>
            <td style="text-align:center; background-color: #4CAF50">regional</td>
            <td style="text-align:center; background-color: #4CAF50">apellido_1</td>
            <td style="text-align:center; background-color: #4CAF50">nombre</td>
            <td style="text-align:center; background-color: #4CAF50">apellido_2</td>
            <td style="text-align:center; background-color: #4CAF50">placa</td>
            <td style="text-align:center; background-color: #4CAF50">nueva_direccion</td>
            <td style="text-align:center; background-color: #4CAF50">nueva_ciudad</td>
            <td style="text-align:center; background-color: #4CAF50">nuevo_departamento</td>
            <td style="text-align:center; background-color: #4CAF50">email</td> 
            <td style="text-align:center; background-color: #4CAF50">nuevo_tel</td> 
            <td style="text-align:center; background-color: #4CAF50">celular</td> 
            <td style="text-align:center; background-color: #4CAF50">valor asegurado</td>           
            <td style="text-align:center; background-color: #4CAF50">deducibles PTD</td>
            <td style="text-align:center; background-color: #4CAF50">deducibles PTH</td>
            <td style="text-align:center; background-color: #4CAF50">fasecolda</td>
            <td style="text-align:center; background-color: #4CAF50">prima_mensual</td>
            <td style="text-align:center; background-color: #4CAF50">prima_anual</td>
            <td style="text-align:center; background-color: #4CAF50">prima_financiada</td>
            <td style="text-align:center; background-color: #4CAF50">valor RCE</td>
            <td style="text-align:center; background-color: #4CAF50">plan</td>
            <td style="text-align:center; background-color: #4CAF50">campaña_top</td>
            <td style="text-align:center; background-color: #4CAF50">Marca</td>
            <td style="text-align:center; background-color: #4CAF50">Modelo</td>
            <td style="text-align:center; background-color: #4CAF50">Producto</td>
            <td style="text-align:center; background-color: #4CAF50">Fecha Inicio Vigencia</td>
            <td style="text-align:center; background-color: #4CAF50">Sarlaft</td>        
        </tr>
    </thead>
    <tbody>
        
        @foreach ($gestiones as $gestion)
            <tr>                  
                <th>{{ is_null($gestion->fecha_venta) ? 'NO APLICA' :  Carbon\carbon::parse($gestion->fecha_venta)->format('d/m/Y')}}</th>
                <th>{{ is_null($gestion->id_asegurado) ? 'NO APLICA' : $gestion->id_asegurado }}</th>
                <th>{{ '' }}</th>
                <th>{{ '' }}</th>
                <th>{{ is_null($gestion->nombre_asegurado) ? 'NO APLICA' : $gestion->nombre_asegurado }}</th>
                <th>{{ '' }}</th>
                <th>{{ is_null($gestion->placa) ? 'NO APLICA' : $gestion->placa }}</th>
                <th>{{ is_null($gestion->direccion) ? 'NO APLICA' : $gestion->direccion }}</th>
                <th>{{ is_null($gestion->ciudad->nombre) ? 'NO APLICA' : $gestion->ciudad->nombre }}</th>
                <th>{{ is_null($gestion->departamento->nombre) ? 'NO APLICA' : $gestion->departamento->nombre }}</th>
                <th>{{ is_null($gestion->email_asegurado) ? 'NO APLICA' : $gestion->email_asegurado }}</th>
                <th>{{ is_null($gestion->telefono_celular) ? 'NO APLICA' : $gestion->telefono_celular }}</th>
                <th>{{ is_null($gestion->telefono_celular) ? 'NO APLICA' : $gestion->telefono_celular }}</th>
                <th>{{ is_null($gestion->valor_asegurado_fasecolda) ? 'NO APLICA' : $gestion->valor_asegurado_fasecolda }}</th>
                <th>{{ '' }}</th>  
                <th>{{ '' }}</th>
                <th>{{ is_null($gestion->fasecolda) ? 'NO APLICA' : $gestion->fasecolda }}</th>
                <th>{{ is_null($gestion->valor_prima_financiada) ? 'NO APLICA' : $gestion->valor_prima_financiada }}</th>
                <th>{{ is_null($gestion->valor_prima_anual) ? 'NO APLICA' : $gestion->valor_prima_anual }}</th>
                <th>{{ is_null($gestion->valor_prima_financiada) ? 'NO APLICA' : $gestion->valor_prima_financiada }}</th>
                <th>{{ is_null($gestion->rc) ? 'NO APLICA' : $gestion->rc }}</th>
                <th>{{ is_null($gestion->tipo_de_plan) ? 'NO APLICA' : $gestion->tipo_de_plan }}</th>
                <th>{{ is_null($gestion->campana) ? 'NO APLICA' : $gestion->campana }}</th>
                <th>{{ is_null($gestion->marca) ? 'NO APLICA' : $gestion->marca }}</th>
                <th>{{ is_null($gestion->modelo) ? 'NO APLICA' : $gestion->modelo }}</th>
                <th>{{ is_null($gestion->producto) ? 'NO APLICA' : $gestion->producto }}</th>
                <th>{{ is_null($gestion->fecha_inicio_de_vigencia) ? 'NO APLICA' : $gestion->fecha_inicio_de_vigencia }}</th>          
                <th>{{ is_null($gestion->metodo_pago) ? 'NO APLICA' : $gestion->metodoPago->item }}</th>        
            </tr>
        @endforeach 
    </tbody>
</table>