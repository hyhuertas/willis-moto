@php
    $telefono=Session::get('telefono');
    $unique_id=Session::get('unique_id');
@endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    

    <title>{{ config('app.name', 'Willis') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    
</head>
<body>
    <div>
        @if (1==5)
        <row>
            <div class="card" style="width: 50rem; margin:10% 0 0 30%">
                <div class="card-body shadow-lg">
                  <h5 class="card-title h1 text-black-50">SIN ACCESO</h5>
                  <p class="card-text h2 text-xl-justify text--darken-1">Comunicate con tu coordinador o supervisor para que te conceda los permisos necesarios.</p>
                </div>
              </div>
        </row>
        @endif
    </div>
    <div id="app">
     <v-app style="width: 100%;" id="inspire">
     <menu-component :rol="{{1}}" :telefono="{{3643304}}" :unique_id="{{16769}}"></menu-component>
     <router-view></router-view>
    </v-app>
    </div>
</body>
</html>
